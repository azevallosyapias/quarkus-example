package org.mazy.example1.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 870869020843197226L;

	private Long id;
	private String name;
	private int age;

}
