package org.mazy.example1;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.mazy.example1.entity.Pet;
import org.mazy.example1.service.IPetService;

@Path("/api/v1/pets")
public class PetController {

	@Inject
	IPetService petService;

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listPets() {

		List<Pet> list = petService.listPets();
		return Response.ok(list).build();
	}
}
