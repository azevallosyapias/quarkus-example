package org.mazy.example1.service;

import java.util.List;

import org.mazy.example1.entity.Pet;

public interface IPetService {

	public List<Pet> listPets();
	public Pet findById(Long id);
	
}
