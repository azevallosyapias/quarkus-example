package org.mazy.example1.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import org.mazy.example1.entity.Pet;

@Singleton
public class PetServiceImpl implements IPetService {

	@Override
	public List<Pet> listPets() {
		List<Pet> pets = new ArrayList<Pet>();
		pets.add(new Pet(1L, "Charlie", 7));
		pets.add(new Pet(2L, "Tarzan", 10));
		return pets;

	}

	@Override
	public Pet findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
